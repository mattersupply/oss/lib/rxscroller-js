# Concept

A single scroll controller (`ScrollController`) is attached to a container and relays scroll messages on requestAnimationFrame to various scroll behaviors.

A basic scroll behavior (`ScrollBehavior`) can be used to listen to linear scroll progress (0 - 1) as the container scrolls. This is your regular cookie cutter solution.

Another concept are using scroll elements (`ScrollElement`) to invoke a scroll action (`ViewportCoordinateScrollAction`). The idea is that as soon as certain elements cross thresholds in the viewport, they trigger a scroll action. This can be used for elements that are part of the normal DOM to invoke action, for example a section after it hits the 50% mark in the viewport.


# Basic Usage

## 
```
  let scrollController = new ScrollController(window, { debug: true })
  this.scrollController = scrollController

  let scrollBehavior = new ScrollBehavior(this.scrollController)
  this.scrollBehavior = scrollBehavior

  this.scrollController.needsResize = true
  this.scrollBehavior.viewport$.subscribe((size) => this.setState({ viewportSize: size }))
  this.scrollBehavior.progress$.subscribe((progress, previous) => this.setState({ scrollProgress: progress }))
```

##
```
  let someAction = new scroller.ViewportCoordinateScrollAction(controller)
  someAction.viewport$.subscribe((newSize) => {
    someAction.scrollOptions = {
      origin: {x: 0, y: '10vh'},
      size: {width: 0, height: '50vh'}
    }
  })

  let anElement = new scroller.ScrollElement(domNode)
  someAction.addElement(anElement)
  someAction.state$.subscribe((a) => {
    if (a.state.y == scroller.STATE_BEFORE) {
      // Do something
    } else {
      // Do something else
    }
  })
```